package models;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import java.util.List;

import play.db.jpa.Model;
import play.data.validation.Required;

@Entity
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"voter", "objectid"}))
public class Vote extends Model
{
    @Required
    public String voter;

    @Required
    public String objectid;

    public Vote ()
    {
    }

    public Vote (String voter, String objectid)
    {
        this.voter = voter;
        this.objectid = objectid;
    }

    @Override
    public boolean equals (Object object)
    {
        if (object == this)
        {
            return true;
        }
        if (!(object instanceof Vote))
        {
            return false;
        }
        Vote vote = (Vote)object;
        if (voter == null && vote.voter != null || !voter.equals(vote.voter))
        {
            return false;
        }
        if (objectid == null && vote.objectid != null || !objectid.equals(vote.objectid))
        {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode ()
    {
        final int prime = 92821;
        int result = 1;
        result = (result * prime) + (voter == null ? 92809 : voter.hashCode());
        result = (result * prime) + (objectid == null ? 92809 : objectid.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "voter=" + voter + ", objectid=" + objectid + "}";
    }
}
