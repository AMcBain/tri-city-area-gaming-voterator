package models;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import play.db.jpa.GenericModel;
import play.data.validation.Required;

@Entity
public class BoardGame extends GenericModel implements Comparable<BoardGame>
{
    @Id
    @Required
    public String objectid;

    @Required
    public String name;

    // The game Go has a yearpublished value of 4294965096.
    // The info page says -2200, so no idea on the API here.
    @Required
    public long yearpublished;

    @Transient
    public int maxplayers;

    @Transient
    public int avgplaytime;

    @OneToMany(mappedBy="objectid", cascade=CascadeType.ALL)
    public Set<Vote> votes = new HashSet<Vote>();

    public BoardGame ()
    {
    }

    public BoardGame (String objectid, String name, long yearpublished)
    {
        this.objectid = objectid;
        this.name = name;
        this.yearpublished = yearpublished;
    }

    public int compareTo (BoardGame b)
    {
        if (b.votes.size() == votes.size())
        {
            return name.compareTo(b.name);
        }
        return b.votes.size() - votes.size();
    }

    public String toString ()
    {
        return "{objectid=" + objectid + ", name=" + name + "}";
    }

    // Wrappers for XStream.
    public static class Collection
    {
        public List<BoardGame> boardgame;

        public Collection (List<BoardGame> list)
        {
            boardgame = list;
        }
    }

    public static class Converter implements com.thoughtworks.xstream.converters.Converter
    {
        public boolean canConvert(Class clazz)
        {
            return clazz.equals(BoardGame.Collection.class);
        }

        public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context)
        {
            // Noop.
        }

        public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context)
        {
            List<BoardGame> games = new ArrayList<BoardGame>();

            while (reader.hasMoreChildren())
            {
                reader.moveDown();

                int minplaytime = 0;
                int maxplaytime = 0;

                BoardGame game = new BoardGame();
                game.objectid = reader.getAttribute("objectid");

                while (reader.hasMoreChildren())
                {
                    reader.moveDown();

                    if ("yearpublished".equals(reader.getNodeName()))
                    {
                        game.yearpublished = Long.parseLong(reader.getValue());
                    }
                    else if ("name".equals(reader.getNodeName()) && "true".equals(reader.getAttribute("primary")))
                    {
                        game.name = reader.getValue();
                    }
                    else if ("maxplayers".equals(reader.getNodeName()))
                    {
                        game.maxplayers = Integer.parseInt(reader.getValue());
                    }
                    else if ("minplaytime".equals(reader.getNodeName()))
                    {
                        minplaytime = Integer.parseInt(reader.getValue());
                    }
                    else if ("maxplaytime".equals(reader.getNodeName()))
                    {
                        maxplaytime = Integer.parseInt(reader.getValue());
                    }

                    reader.moveUp();
                }
                game.avgplaytime = (minplaytime + maxplaytime) / 2;

                reader.moveUp();
                games.add(game);
            }

            return new Collection(games);
        }
    }
}
