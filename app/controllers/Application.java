package controllers;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.javabean.JavaBeanConverter;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;

import java.io.InputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;
import static java.nio.charset.StandardCharsets.UTF_8;

import models.BoardGame;
import models.Vote;

import org.apache.commons.io.IOUtils;

import play.cache.Cache;
import play.jobs.Job;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Router;

public class Application extends Controller
{
    private static final XStream parser;
    static
    {
        parser = new XStream(new StaxDriver());
        parser.registerConverter(new BoardGame.Converter(), 42);
        parser.alias("boardgames", BoardGame.Collection.class);
        parser.alias("boardgame", BoardGame.class);
    }

    public static String getUser ()
    {
        String user = params.get("user", String.class);
        if (user == null)
        {
            Http.Header header = request.headers.get("x-forwarded-for");
            return (header != null && header.values.size() > 0 ? header.values.get(0) : request.remoteAddress);
        }
        return user;
    }

    public static void index (boolean sort)
    {
        String user = getUser();
        List<BoardGame> games = BoardGame.findAll();

        if (sort)
        {
            Collections.sort(games);
        }
        render(games, user, sort);
    }

    public static void randomUser ()
    {
        redirect(Router.reverse("Application.index", Collections.<String, Object>singletonMap("user", UUID.randomUUID().toString())).url);
    }

    public static Job<List<BoardGame>> fetch (final String path)
    {
        return new Job<List<BoardGame>> ()
        {
            @Override
            public List<BoardGame> doJobWithResult () throws Exception
            {
                List<BoardGame> games = Collections.emptyList();

                InputStream is = null;
                try
                {
                    URL url = new URL("https://www.boardgamegeek.com/xmlapi/" + path);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("method", "GET");
                    connection.setDoInput(true);
                    connection.setUseCaches(false);
                    connection.setConnectTimeout(3000);
                    connection.setReadTimeout(60000);
                    connection.connect();

                    if (connection.getResponseCode() == 200)
                    {
                        String input = IOUtils.toString(is = connection.getInputStream(), UTF_8);
                        games = ((BoardGame.Collection)parser.fromXML(input)).boardgame;
                    }
                }
                finally
                {
                    if (is != null)
                    {
                        is.close();
                    }
                }

                return games;
            }
        };
    }

    public static void search (String text)
    {
        try
        {
            List<BoardGame> games = await(fetch("search?search=" + URLEncoder.encode(text, "UTF-8")).now());

            if (games == null)
            {
                games = Collections.emptyList();
            }
            else
            {
                StringBuilder builder = new StringBuilder();
                builder.append("boardgame/");

                for (int i = 0, max = Math.min(12, games.size()); i < max; i++)
                {
                    builder.append(games.get(i).objectid);
                    builder.append(",");
                }

                games = new ArrayList<BoardGame>();
                for (BoardGame game : await(fetch(builder.deleteCharAt(builder.length() - 1).toString()).in(1)))
                {
                    if (game.avgplaytime >= 45 && game.maxplayers > 2)
                    {
                        Cache.add(game.objectid, game);
                        games.add(game);
                    }
                }
            }

            renderJSON(games);
        }
        catch (UnsupportedEncodingException e)
        {
            error("The world has ended. You are dead. In fact, seeing as you're dead, you aren't reading this.");
        }
    }

    public static void voteAdd (String objectid)
    {
        String user = getUser();
        Vote vote = Vote.find("objectid = ?2 and voter = ?1", user, objectid).first();

        if (vote == null)
        {
            BoardGame game = BoardGame.find("objectid = ?1", objectid).first();
            game.votes.add(new Vote(user, objectid));
            game.save();
        }

        List<BoardGame> games = BoardGame.findAll();
        renderTemplate("Application/games.html", games, user);
    }

    public static void voteDel (String objectid)
    {
        String user = getUser();
        Vote vote = Vote.find("objectid = ?2 and voter = ?1", user, objectid).first();
        notFoundIfNull(vote);
        vote.delete();

        List<BoardGame> games = BoardGame.findAll();
        renderTemplate("Application/games.html", games, user);
    }

    public static void gameAdd (String objectid)
    {
        String user = getUser();
        BoardGame boardGame = BoardGame.find("objectid = ?1", objectid).first();

        // Potentially unsafe about the Cache here with threading, but games can only be added (currently)
        // from the datalist so they had to have been looked up and cached before. This is really just an
        // "Are we really getting a valid ID here?" without having to ask BoardGameGeek to verify.
        if (boardGame == null && Cache.get(objectid) != null)
        {
            boardGame = Cache.get(objectid, BoardGame.class);
            boardGame.save();
        }

        List<BoardGame> games = BoardGame.findAll();
        renderTemplate("Application/games.html", games, user);
    }
}
