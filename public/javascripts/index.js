$(function ()
{
    var timeout, request, value;

    if (typeof lookup === "undefined")
    {
        return;
    }

    $("#list").on("click", "li", function (event)
    {
        var item = $(event.target).closest("[data-id]");

        if (item.length && !$(event.target).is("a"))
        {
            $.ajax({
                method: "POST",
                url: "vote/" + (item.hasClass("voted") ? "no" : "yes") + location.search,
                data: {
                    objectid: item.data("id")
                },
                success: function (data)
                {
                    // Bad idea. Could allow injections if not deployed with a certificate. Could lead to burnt
                    // airships and lost hats. Any plan where you lose your hat is a bad plan. Just deploy this
                    // app with a certificate.
                    list.innerHTML = data;
                }
            });
        }
    });

    $(datalist).on("click", function (event)
    {
        var item = $(event.target);

        if (item.data("id"))
        {
            lookup.value = item.find(".name").text();
            $(lookup).trigger("input");
        }
    });

    function escapeHTML (value)
    {
        return value.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/'/g, "&#39;");
    }

    function escapeCSS (value)
    {
        return value.replace(/'/g, "\\'");
    }

    function lookupValue ()
    {
        return lookup.value.toLowerCase().trim();
    }

    $(lookup).on("input", function ()
    {
        var latest = lookupValue(), option = datalist.querySelector("[data-value='" + escapeCSS(latest) + "']");

        if (option)
        {
            lookup.className = "";
            makeitso.disabled = false;
            datalist.style.display = "";
        }
        else if (latest != value && latest.length)
        {
            value = latest;
            makeitso.disabled = true;

            clearTimeout(timeout);
            timeout = setTimeout(function ()
            {
                if (request)
                {
                    request.abort();
                }

                request = $.ajax({
                    method: "POST",
                    url: "search" + location.search,
                    data: {
                        text: value
                    },
                    success: function (data)
                    {
                        if (data.length)
                        {
                            lookup.className = "datalist";
                            datalist.style.display = "block";
                            datalist.style.width = lookup.offsetWidth + lookup.nextElementSibling.offsetWidth + "px";
                            datalist.innerHTML = data.map(function (data)
                            {
                                var name = escapeHTML(data.name);

                                return "<li data-id='" + escapeHTML(data.objectid) + "' data-value='" +
                                        name.toLowerCase() + "'><span class='name'>" + name + "</span> (" +
                                        escapeHTML(data.yearpublished + "")  + ")</li>";
                            }).join("");
                        }
                        else
                        {
                            lookup.className = "";
                            datalist.style.display = "";
                        }
                    },
                    error: function ()
                    {
                        lookup.className = "";
                        datalist.innerHTML = "";
                    }
                });
            }, 375);
        }
        else if (!latest.length)
        {
            lookup.className = "";
            makeitso.disabled = true;
            datalist.innerHTML = "";
            value = latest;

            if (request)
            {
                request.abort();
            }
            clearTimeout(timeout);
        }
    }).val("");

    /* From http://stackoverflow.com/a/8809472/251262 */
    function uuid ()
    {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c)
        {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    }
    /* End borrowing. */

    $(makeitso).on("click", function ()
    {
        var option = datalist.querySelector("[data-value='" + escapeCSS(lookupValue()) + "']");

        if (option)
        {
            $.ajax({
                method: "POST",
                url: "suggest" + location.search,
                data: {
                    user: location.search.indexOf("random") !== -1 && uuid() || undefined,
                    objectid: option.dataset.id
                },
                success: function (data)
                {
                    lookup.value = "";
                    makeitso.disabled = true;

                    // Also a bad idea. See previous comment.
                    list.innerHTML = data;
                }
            });
        }
    }).prop("disabled", true);

    if (location.search.indexOf("random") !== -1)
    {
        $(".fa-thumbs-up").remove();
        $(".voted").removeClass("voted");
    }
});
